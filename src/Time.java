import java.util.Scanner;

public class Time {
    private int hours;
    private int minutes;
    private int seconds;

    public Time() {
        this.hours = 0;
        this.minutes = 0;
        this.seconds = 0;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
        if (hours >= 24 || hours < 0){
            this.hours = 0;
        }
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
        if (minutes >= 60 || minutes < 0){
            this.minutes = 0;
        }
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
        if (seconds >= 60 || seconds < 0){
            this.seconds = 0;
        }
    }
    public void settingTheTime(){
        System.out.println("Set the time");
        Scanner scanner = new Scanner(System.in);
        int hours = scanner.nextInt();
        this.setHours(hours);
        int minutes = scanner.nextInt();
        this.setMinutes(minutes);
        int seconds = scanner.nextInt();
        this.setSeconds(seconds);
    }
    public void settingHours(){
        System.out.println("Set hours");
        Scanner scanner = new Scanner(System.in);
        int hours = scanner.nextInt();
        this.setHours(hours);
    }
    public void settingMinutes(){
        System.out.println("Set minutes");
        Scanner scanner = new Scanner(System.in);
        int minutes = scanner.nextInt();
        this.setMinutes(minutes);
    }
    public void settingSeconds(){
        System.out.println("Set seconds");
        Scanner scanner = new Scanner(System.in);
        int seconds = scanner.nextInt();
        this.setSeconds(seconds);
    }

    public void display(){
        System.out.format("%02d", this.getHours());
        System.out.print(":");
        System.out.format("%02d", this.getMinutes());
        System.out.print(":");
        System.out.format("%02d", this.getSeconds());
        System.out.println();
    }
}
